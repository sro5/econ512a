%Question 1%
X = [1;1.5;3;4;5;7;9;10]
Y1 = -2 +.5*X;
Y2 = -2 + .5*X.^2;
plot(X,Y1,X,Y2)

%Question 2%
X = linspace(-10,20,200);
X = sum(X');

%Question 3%
A = [2,4,6;1,7,5;3,12,4];
B = [-2;3;10];
C = A'*B;
D = (A'*A)\B;
E = sum(sum(A.*B));

F = A;
F(2,:) = [];
F(:,3) = [];

x = linsolve(A,B);

%Question 4%
Acell =  repmat({A}, 1, 5);
B = blkdiag(Acell{:});

%Question 5%
A = randn(5,3) + 10;
B = zeros(size(A));

% it is easier to do B = A<10
for i = 1:numel(A)
    if A(i) >= 10
        B(i) = 1;
    else
        B(i) = 0;
    end
end
%or
A = randn(5,3) + 10;
A(A<10)=0
A(A>=10)=1

%Question 6%
data = csvread('datahw1.csv');
Y = [data(:,5)];
X = [ones(size(Y)),data(:,3),data(:,4),data(:,6)];
n = length(Y);
k = 4;

OLS = (X'*X)\(X'*Y);
e = Y-X*OLS;
var = e'*e/(n-k);
se = sqrt(var*diag(inv(X'*X)));
t = OLS./se;
p = 2*(1-tcdf(abs(t), n-k));
