addpath('CEtools');
va=-1;
vb=-1;
vc=-1;

pa=1;
pb=1;
pc=1;

vj=[va, vb, vc];
pj=[pa, pb, pc];

qa=exp(va-pa)/(1+exp(va-pa)+exp(vb-pb)+exp(vc-pc));
qb=exp(vb-pb)/(1+exp(va-pa)+exp(vb-pb)+exp(vc-pc));
qc=exp(vc-pc)/(1+exp(va-pa)+exp(vb-pb)+exp(vc-pc));
q0=1/(1+exp(va-pa)+exp(vb-pb)+exp(vc-pc));
q=[qa, qb, qc, q0]

%% #1 Solutions
% qa =
% 
%     *0.0963*
%     
% qb =
% 
%     *0.0963*
% 
% 
% qc =
% 
%     *0.0963*
% 
% 
% q0 =
% 
%     *0.7112*    
%% 2

% Set options for Broyden
optset('broyden','showiters',1000);
optset('broyden','maxit',300) ;
optset('broyden','tol',1e-8) ;

f = @(P) [exp(-1-P(1))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3))) - ...
        P(1)*exp(-1-P(1))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)))...
        *(1-exp(-1-P(1))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3))));...
            exp(-1-P(2))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3))) - ...
            P(2)*exp(-1-P(2))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)))...
            *(1-exp(-1-P(2))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3))));...
                exp(-1-P(3))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3))) - ...
                P(3)*exp(-1-P(3))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)))...
                *(1-exp(-1-P(3))/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3))))];

% this is a common mistake with you, Punya Chatterjee and Maxence Valentin.
% dont be so similar with your homeworks or I will not accept your
% homeworks next time. 

% this is the profit of a firm, not a FOC of it. You are supposed to solve
% for FOC == 0 not profit equals zero.

%Starting value 1,1,1%
tic
a=1;
b=1;
c=1;
P_start=[a,b,c];
disp('Starting value')
P_start
            
P = broyden(f,[1;1;1]);

disp('Solutions P')
P
disp('Value of functions')
f(P)
toc

%Starting value 0,0,0%
tic
a=0;
b=0;
c=0;
P_start=[a,b,c];
disp('Starting value')
P_start
           
P = broyden(f,[0;0;0]);

disp('Solutions P')
P
disp('Value of functions')
f(P)
toc

%Starting value 0,1,2%
tic
a=0;
b=1;
c=2;
P_start=[a,b,c];
disp('Starting value')
P_start
            
P = broyden(f,[0;1;2]);

disp('Solutions P')
P
disp('Value of functions')
f(P)
toc


%Starting value 3,2,1%
tic
a=3;
b=2;
c=1;
P_start=[a,b,c];
disp('Starting value')
P_start
  
P = broyden(f,[3;2;1]);

disp('Solutions P')
P
disp('Value of functions')
f(P)
toc
%The last two broyden routines return values of P which are inconsistent
%with the Nash equilibrium conditions. 

%% 4
tol = 1e-8;

%Starting value 1,1,1%
tic

a=1;
b=1;
c=1;
it=1;

P=[a,b,c];
disp('Starting value')
Pnew = [0,0,0];
P
while abs(P-Pnew)>tol
 

Q=exp(-1-P)/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)));

Pnew=1./(1.-Q);
P=Pnew;
it = it+1;
if it>=100
    break
end  
end
disp('Number of outter loop')
it
disp('Solutions P')
P
disp('Value of functions')
Q
toc

%Starting value 0,0,0%
tic

a=0;
b=0;
c=0;
it=1;
Pnew = [0,0,0];

P=[a,b,c];
disp('Starting value')
P
while abs(P-Pnew)>tol
% abs(P-Pnew) is vector-valued function and it is equal exactly zero at the
% start. it does not run even once. you were supposed to notice that 
P=Pnew;
Q=exp(-1-P)/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)));

Pnew=1./(1.-Q);


it = it+1;
if it>=100
    break
end  
end
disp('Number of outter loop')
it
disp('Solutions P')
Pnew
disp('Value of functions')
Q
toc

%Starting value 0,1,2%
tic

a=0;
b=1;
c=2;
it=1;
Pnew = [0,0,0];

P=[a,b,c]
disp('Starting value')
P
while abs(P-Pnew)>tol
P=Pnew;
Q=exp(-1-P)/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)));

Pnew=1./(1.-Q);


it = it+1;
if it>=100
    break
end  
end
disp('Number of outter loop')
it
disp('Solutions P')
Pnew
disp('Value of functions')
Q
toc

%Starting value 3,2,1%
tic

a=3;
b=2;
c=1;
it=1;
Pnew = [0,0,0];

P=[a,b,c];
disp('Starting value')
P
while abs(P-Pnew)>tol
P=Pnew;
Q=exp(-1-P)/(1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3)));

Pnew=1./(1.-Q);

it = it+1;
if it>=10000
    break
end  
end
disp('Number of outter loop')
it
disp('Solutions P')
Pnew
disp('Value of functions')
Q
toc

%All routines converge to P = 1.0985 from each starting point
%They are similar to the Broyden method results when starting with 
%reasonable start values.
